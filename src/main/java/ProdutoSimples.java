import java.io.IOException;
import java.util.Scanner;

public class SomaSimples {

    public static void main(String [] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        int SOMA = A + B;
        System.out.println("SOMA = " + SOMA);
    }

}
