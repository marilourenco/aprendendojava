import java.io.IOException;
import java.util.Scanner;

public class ProdutoSimples {

    public static void main(String [] args) throws IOException {
        Scanner in = new Scanner(System.in);
        double A = in.nextDouble();
        double B = in.nextDouble();
        double peso1 = 3.5;
        double peso2 = 7.5;
        double somaPesos = 11.0;
        double MEDIA = ((A * peso1) + (B * peso2)) / somaPesos;
        System.out.println(String.format("MEDIA = %.5f", MEDIA));
    }

}
