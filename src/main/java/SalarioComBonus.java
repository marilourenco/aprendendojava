import java.io.IOException;
import java.util.Scanner;

public class Salario {

    public static void main(String [] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int numeroFuncionario = in.nextInt();
        int horasTrabalhadas = in.nextInt();
        double entradasalario = in.nextDouble();
        double salariototal = horasTrabalhadas * entradasalario;
        System.out.println("NUMBER = " + numeroFuncionario + "\n" + String.format("SALARY = U$ %.2f", salariototal));
    }

}