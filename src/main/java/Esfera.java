import java.io.IOException;
import java.util.Scanner;

public class AreaDoCirculo {

    public static void main(String [] args) throws IOException {
        Scanner in = new Scanner(System.in);
        double n = 3.14159;
        double raio = in.nextDouble();
        double area = n * (Math.pow(raio, 2));
        System.out.println(String.format("A=%.4f", area));
    }

}
