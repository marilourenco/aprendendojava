import java.io.IOException;
import java.util.Scanner;

public class AreaDeFigurasGeometricas {

    public static void main(String [] args) throws IOException {
        Scanner in = new Scanner(System.in);
        double A = in.nextDouble();
        double B = in.nextDouble();
        double C = in.nextDouble();
        double pi  = 3.14159;
        double areaDoTrianguloRetangulo = (A * C) / 2;
        double areaDoCirculo = (Math.pow(C, 2)) * pi;
        double areaDoTrapezio = (A + B) * C / 2;
        double areaDoQuadrado = B * B;
        double areaDoRetangulo = A * B;

        System.out.println(String.format("TRIANGULO: %.3f", areaDoTrianguloRetangulo));
        System.out.println(String.format("CIRCULO: %.3f", areaDoCirculo));
        System.out.println(String.format("TRAPEZIO: %.3f", areaDoTrapezio));
        System.out.println(String.format("QUADRADO: %.3f", areaDoQuadrado));
        System.out.println(String.format("RETANGULO: %.3f", areaDoRetangulo));
    }

}
