import java.io.IOException;
import java.util.Scanner;

public class ConsumoGasolina {

    public static void main(String [] args) throws IOException {
        Scanner in = new Scanner(System.in);
        int distanciaTotalPercorrida = in.nextInt();
        double totalCombustivelGasto = in.nextDouble();
        double consumoMedio = distanciaTotalPercorrida / totalCombustivelGasto;
        System.out.println(String.format("%.3f km/l", consumoMedio));
    }

}
